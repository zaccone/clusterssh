from log import log

class Config(object):
    """
    Base class responsible for storing the configuration and
    yielding the configuration dictionaries on demand.
    Config subclasses should be able to read files,open databases,
    wait on network and so on.
    """
    def __init__(self,*args,**kwargs):
        self._cfg = args
        self._globals = kwargs
        log.debug("GLOBALS: %s" % kwargs)

    def __len__(self):
        return len(self._cfg)

    def __str__(self):
        return "Configuration class %s, with %d hosts to be processed" \
                % (self.__class__.__name__, len(self._cfg))

    def _output_setup(self):
        if 'write' in self._globals:
            filename = self._globals['write']
            log.enableStream(flag=True)
            log.outputfile(filename)

    def _verbose(self):
        flag = self._globals['verbose']
        log.enableStream(flag=flag)

#    # not used for now
#    @staticmethod
#    def cfg(*args):
#        TYPES = ('input','default')
#        CLASSES = {'input':FileConfig,
#                  }
#        for t in Config.TYPES:
#            if t in args:
#                return CLASSES[t]
#        else:
#            return StdinConfig
#

class StdinConfig(Config):
    """
    Config class used for parsing options specified in the command line.
    Please see Parser class code

    WISHLIST:
     - add config validation and error raising
    """
    PARAMETERS = ('user','password','key','command','script')

    def __init__(self,hosts,**globals):
        super(StdinConfig,self).__init__(hosts,**globals)
        self._hosts = hosts
        self._default_params_dict = None

    def _default_params(self):
        check_none = lambda x: x is not None
        globals = self._globals
        if not self._default_params_dict:
            d=dict([(param,globals.get(param) ) for param in StdinConfig.PARAMETERS
                    if check_none(globals.get(param))])
            self._default_params_dict = d

        return self._default_params_dict
    
    def _parse(self,host):
        r = self._default_params()
        r['host'] = host
        return r

    def _command(self):
        def _read_command(path):
            try:
                with open(path,'r') as f:
                    return f.read()
            except IOError,e:
                log.die("I/O error while handling input script: %s" % e)

        params = self._default_params_dict

        if not any([params.get(x,None) for x in ['command','script']]):
            msg = ("Options --script/--command are empty, are you sure what ",
                    "you are doing?")
            log.warning(msg)

        if 'command' in params and params['command']:
            return
        elif 'script' in params:
            params['command'] = _read_command(params['script'])
            
    def setup(self):
        self._default_params()
        self._verbose()
        self._command()
        self._output_setup()

    def host_cfg(self):
        for host in self._hosts:
            yield self._parse(host)

class FileConfig(Config):
    """
    Config class used for parsing file and creating hosts configurations
    including user,host,password or public/private key
    """
    DELIMITER = '\t'
    PARAMETERS = ('host','user','password','key','command')

    def __init__(self,*args,**globals):
        super(FileConfig,self).__init__(*args,**globals)
        self._filename = args[0]

    def _open(self):
        try:
            with open(self._filename,'r') as f:
                for line in f:
                    yield line
        except IOError,e:
            log.error("Problems with file %s, %s" % (filename,e))

    def _parse(self,line):
        """
        Parse single config line and return a dictionary with the values.
        Example of the file may look like:
        <Host/IP>\t<User>\t<Password>\t<Path to the private Key>\t<Command/Script>
        Lines starting with '#' sign are discarded, so feel free to put your comments
        there.
        """
        if line.startswith('#') or line.startswith(' '):
            return None
        line = line.rstrip(' \n')
        parameters = line.split(FileConfig.DELIMITER,len(FileConfig.PARAMETERS))

        fn = lambda (name,param): ( (name,param.strip()) if param
                     else (name,self._globals.get(name,'')) )

        return dict(map(fn,zip(FileConfig.PARAMETERS,parameters)))

    def setup(self):
        self._cfg = [self._parse(line) for line in self._open() if line]

    def host_cfg(self):
        for cfg in self._cfg:
            yield cfg

## TESTS

if __name__ == "__main__":
    cfg = FileConfig('/tmp/input')
    cfg.setup()
    for cfg in cfg.host_cfg():
       print cfg
