
import multiprocessing
import logging
import sys
import time

__lock = multiprocessing.Lock()

def checknone(fn):
    def wrapper(*args,**kwargs):
        if not all(args) and not all(kwargs.values()):
            return None
        return fn(*args,**kwargs)
    return wrapper

def synchronized(fn):
    def wrapper(*args,**kwargs):
        with __lock:
            return fn(*args,**kwargs)
    return wrapper

class Log(object):
    """Wrapper for logging Python module"""

    DATE_FORMAT = '%d/%m/%Y %I:%M:%S'
    LOG_FORMAT = '%(asctime)s %(levelname)s: %(message)s'
    PURE_FORMAT = '%(message)s'

    @staticmethod
    def __str_to_loglevel__(loglevel):
        levels = {'DEBUG':logging.DEBUG,
                  'INFO':logging.INFO,
                  'WARNING':logging.WARNING,
                  'ERROR':logging.ERROR,
                  'CRITICAL':logging.CRITICAL
                 }

        loglevel = loglevel.upper()
        try:
            return levels[loglevel]
        except KeyError:
            raise AttributeError("Unrecognized logging level: %s" % loglevel)

    def __init__(self,loglevel='INFO'):
        self.log_formatter = logging.Formatter(fmt=Log.LOG_FORMAT,datefmt=Log.DATE_FORMAT)
        self.pure_formatter = logging.Formatter(fmt=Log.PURE_FORMAT)

        self.logger = logging.getLogger('remotecluster')
        self.logger.setLevel(Log.__str_to_loglevel__(loglevel))
        self.log_handler = logging.StreamHandler()
        self.log_handler.setFormatter(self.log_formatter)
        self.logger.addHandler(self.log_handler)

        self.stream = logging.getLogger('stdout')
        self.stream.setLevel(logging.ERROR)
        self.stream_handler = logging.StreamHandler(sys.stdout)
        self.stream_handler.setFormatter(self.pure_formatter)
        self.stream.addHandler(self.stream_handler)

        self._messages = multiprocessing.Queue

    def setLevel(self,loglevel):
        loglevel_ = Log.__str_to_loglevel__(loglevel)
        self.logger.setLevel(loglevel_)
        self.logger.debug('Setting loglevel to %s' % loglevel)

    def enableStream(self,flag=False):
        level = logging.INFO if flag is True else logging.ERROR
        self.stream.setLevel(level)

    @checknone
    def logfile(self,filename):
        fh = None
        try:
            fh = logging.FileHandler(filename)
        except IOError,e:
            self.error(e)
        self.logger.removeHandler(self.log_handler)
        fh.setFormatter(self.log_formatter)
        self.logger.addHandler(fh)

    @checknone
    def outputfile(self,filename):
        fh = None
        try:
            fh = logging.FileHandler(filename)
        except IOError,e:
            self.error(str(e))
        else:
            self.stream.removeHandler(self.stream_handler)
            fh.setFormatter(self.pure_formatter)
            self.stream.addHandler(fh)

    def say(self,message):
        self.stream.info(message)

    def debug(self,message):
        self.logger.debug(message)

    def info(self,message):
        self.logger.info(message)

    def warning(self,message):
        self.logger.warning(message)

    def error(self,message):
        self.logger.error(message)
    
    def critical(self,message):
        self.logger.critical(message)
    
    def die(self,message,err=1):
        self.logger.critical("%s\nExiting..." % message)
        sys.exit(err)

log = Log('INFO')

