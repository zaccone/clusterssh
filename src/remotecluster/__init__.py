import signal

import pyev

from client import SSHClient
from log import log
from config import Config, FileConfig

class RemoteCluster(object):
    def __init__(self,cfg,gcfg=None):
        self._cfg = cfg
        self._gcfg = gcfg

        self._loop = pyev.default_loop()
        self._watchers = []
        self._clients = set([])

        self._loop.data = set([])
        self._cfg.setup()
        self._setup_signal_watcher()
        self._setup_idle_watcher()

    def _setup_idle_watcher(self):
        log.debug("Setup async watcher")
        self._async = pyev.Async(self._loop,self._async_cb)
        self._async.start()

    def _setup_signal_watcher(self):
        log.debug("Setup signal watcher")
        self._signals = pyev.Signal(signal.SIGINT,self._loop, self._signal_cb)
        self._signals.start()

    def _signal_cb(self,watcher,revents):
        if revents & pyev.EV_SIGNAL:
            return self._handle_signal(watcher,revents)
        log.error("Got non signal event for signal handler")

    def _handle_signal(self,watcher,revents):
        log.say("Caught signal, stopping all clients and exiting...")
        log.say("Managing already finished tasks")
        self._async.stop()
        while self._loop.data:
            client = self._loop.data.pop()
            #self._clients.remove(client)
        log.say("Managing ongoing clients")
        while self._clients:
            print "clients", str(self._clients)
            client = self._clients.pop()

            client.stop()
            self._report(client)

        self._loop.stop(pyev.EVBREAK_ALL)
        log.say("Bye.")

    def _async_cb(self,watcher,revents):
        while self._loop.data:
            client = self._loop.data.pop()
            self._report(client)
            self._clients.remove(client)
        if not self._clients:
            self._async.stop()
            self._loop.stop(pyev.EVBREAK_ALL)

    def run(self):
        for cfg in self._cfg.host_cfg():
            client = SSHClient(self._loop,self._async)
            if client.prepare(cfg):
                self._clients.update([client])
                client.start()
            else:
                # TODO(marek): should warn user
                del client
        if self._clients:
            self._loop.start()
        else:
            pass
            #self.report()

    def report(self):
        if not self._clients:
            log.say("Nothing to report for %d hosts" % len(self._cfg))
            return 

        for client in self._clients:
            self._report(client)

    def _report(self,client):
        log.say("%s output: %s" %(client, client.output))
        client.close_output()
