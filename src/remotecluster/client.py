import socket
import StringIO

import remotecluster.exceptions as exceptions

from log import log

try:
    import pyev
except ImportError:
    msg = ' '.join(("Cannot import module pyev,",
                    "please take a look at",
                    "https://code.google.com/p/pyev/"))
    log.die(msg)

try:
    import paramiko
except ImportError:
    msg = ' '.join(("Cannot import module paramiko,",
                   "please download from:",
                   "https://github.com/paramiko/paramiko"))
    log.die(msg)


class Client(object):
    def __init__(self,loop,async,*args,**kwargs):
        self._loop = loop
        self._async = async
        self._watcher = None

class SSHClient(Client):

    def __init__(self,loop,async,*args,**kwargs):
        super(SSHClient,self).__init__(loop,async,*args,**kwargs)
        self._ssh = paramiko.SSHClient()
        self._ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self._channel = None
        self._output = StringIO.StringIO()
        self._user, self._host = '', ''
        self._method = 'unknown'
        self._methods = dict({'password':'password', 'key':'key_filename'})

    def __str__(self):
        return "%s %s@%s" % (self.__class__.__name__, self._user, self._host)

    def _io_cb(self, watcher, revents):
        if revents & pyev.EV_READ:
            self._handle_read()
        else:
            log.debug("Got non pyev.EV_READ event")

    def _handle_read(self):
        buf = self._channel.recv(1024)
        if buf:
            self._output.write(buf)
        else:
            msg = ("%s: closing connection,exit code: %d" %
                  (self,self._channel.recv_exit_status()))
            log.info(msg)
            self.stop()
            self.trigger()

    @property
    def output(self):
        return self._output.getvalue()

    def _login_args(self,cfg):
        for method in sorted(self._methods.keys()):
            if method in cfg:
                self._method = method
                return dict({self._methods[method] : cfg[method]})
        else:
            msg = ("Cannot find none of available login methods: %s" %
                  ','.join(self._methods.keys()))
            raise exceptions.LoginMethodError(msg)



    def prepare(self,cfg,*args,**kwargs):

        host,user, command = [cfg[x].strip(' "') for x in ['host','user','command']]
        self._user, self._host = user, host
        try:
            login_args = self._login_args(cfg)
            self._ssh.connect(host,username=user,**login_args)
            log.info("%s: connected" % self)
            log.debug("command to be executed: %s" % command)
            _,stdout,_ = self._ssh.exec_command(command)
            self._channel = stdout.channel
            self._channel.set_combine_stderr(True)
        except socket.error,e:
            log.warning("Socket error for host %s: %s" % (host,e))
            return False
        except exceptions.LoginMethodError,e:
            log.warning(e)
            return False
        except paramiko.SSHException,e:
            msg = "%s: cannot login, method %s, reason: %s" % (self,self._method,e)
            log.warning(msg)
            self._ssh.close()
            return False
        else:
            return True

    def start(self):
        self._watcher = pyev.Io(self._channel, pyev.EV_READ, self._loop, self._io_cb)
        self._watcher.start()

    def trigger(self):
        self._loop.data.update( [self] )
        self._async.send()

    def stop(self):
        try:
            self._watcher.stop()
        finally:
            self._channel.close()
            self._ssh.close()

    def close_output(self):
        self._output.close()

