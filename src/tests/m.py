#!/usr/bin/python

from log import log as LOG
import multiprocessing

NB = 10

def f(x):
    if x%2:
        LOG.say(x)
    else:
        LOG.info(x)



if __name__ == '__main__':
    LOG.outputfile('/tmp/threadedlog')
    pool = multiprocessing.Pool(processes=NB)
    pool.map(f,range(NB))
